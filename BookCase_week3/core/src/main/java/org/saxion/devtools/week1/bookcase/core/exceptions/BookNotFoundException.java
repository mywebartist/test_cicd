package org.saxion.devtools.week1.bookcase.core.exceptions;

public class BookNotFoundException extends Exception {

    public BookNotFoundException(String msg) {
        super(msg);
    }

}
