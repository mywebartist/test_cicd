const bodyParser = require('body-parser');
const express = require('express');
const TodoList = require('./todo.js');
const cors = require('cors');
const app = express();

// Allow requests from other domains (cross-origin resource sharing)
app.use(cors());

// Setup the body parser
app.use(bodyParser.json());

// Data store (in memory)
const todoList = new TodoList();

/**
 * @api {get} /api/todo REST routings for the ToDo list
 * @apiName gettodo
 * @apiGroup todo
 * @apiSampleRequest url
 * REST routings for the ToDo list
 */
app.get('/api/todo', function(req, res) {
  res.status(200);
  res.json(todoList.getList());
});

/**
 * @api {post} /api/todo/:todoItem REST routings for the ToDo list
 * @apiName posttodo
 * @apiGroup todoitem
 * @apiParam {string} put something here
 */
app.post('/api/todo/:todoItem', function(req, res) {
  try {
    todoList.add(req.params.todoItem);
    res.status(201);
    res.end();
  } catch (err) {
    res.status(400);
    res.end();
  }
});

/**
 * @api {delete} /api/todo/:totoItem REST routings for the ToDo list
 * @apiName deletetodo
 * @apiGroup todoitem
 * @apiParam {string} put something here
 */
app.delete('/api/todo/:todoItem', function(req, res) {
  try {
    todoList.delete(req.params.todoItem);
    res.status(204);
    res.end();
  } catch (err) {
    res.status(404);
    res.end();
  }
});

module.exports = app;
